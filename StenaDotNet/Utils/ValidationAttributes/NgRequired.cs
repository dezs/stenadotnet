﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace StenaDotNet.Utils.ValidationAttributes
{
    public class NgRequiredAttribute : RequiredAttribute, IMetadataAware
    {

        public void OnMetadataCreated(ModelMetadata metadata)
        {
            metadata.AdditionalValues["required"] = true;
        }
    }
}