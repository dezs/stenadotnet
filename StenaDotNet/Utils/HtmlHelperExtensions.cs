﻿using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Linq.Expressions;
using System;
using System.Web.Mvc.Html;
using System.Collections.Generic;

namespace StenaDotNet.Utils
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlString ToRawJson(this HtmlHelper helper, object obj)
        {
            return helper.Raw(JsonConvert.SerializeObject(obj));
        }

        public static IHtmlString NgTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, 
            Expression<Func<TModel, TProperty>> expression, string ngModelName)
        {
            return NgTextBoxForInternal(htmlHelper, expression, ngModelName, new Dictionary<string, object>());
        }

        public static IHtmlString NgNumericTextBoxFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, string ngModelName)
        {
            return NgTextBoxForInternal(htmlHelper, expression, ngModelName, new Dictionary<string, object>
            {
                {"type", "number" }
            });
        }

        private static IHtmlString NgTextBoxForInternal<TModel, TProperty>(HtmlHelper<TModel> htmlHelper,
            Expression<Func<TModel, TProperty>> expression, string ngModelName, IDictionary<string, object> htmlAttributes)
        {
            var name = htmlHelper.NameFor(expression).ToHtmlString();
            var metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);

            if (metadata.AdditionalValues.ContainsKey("required"))
            {
                htmlAttributes.Add("required", null);
            }


            htmlAttributes.Add("ng-model", ngModelName + "." + name);

            if (htmlAttributes.ContainsKey("class"))
            {
                htmlAttributes["class"] += " form-control";
            }
            else
            {
                htmlAttributes.Add("class", "form-control");
            }

            return htmlHelper.TextBoxFor(expression, htmlAttributes);
        }
    }
}