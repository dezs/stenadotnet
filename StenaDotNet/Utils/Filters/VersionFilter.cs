﻿using NLog;
using StenaDotNet.InternalServices.Interfaces;
using System.Web.Mvc;

namespace StenaDotNet.Utils.Filters
{
    public class VersionFilter : ActionFilterAttribute
    {
        private readonly IVersionService versionService;
        private readonly ILogger logger;

        public VersionFilter(IVersionService versionService, ILogger logger)
        {
            this.versionService = versionService;
            this.logger = logger;
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {
            context.Controller.ViewBag.Version = versionService.GetVersion();
            base.OnResultExecuting(context);
        }
    }
}