﻿using Services.Utility;
using System.Web;
using System.IO;
using System.Threading.Tasks;
using System.Web.Hosting;

namespace StenaDotNet.InternalServices.Implementation
{
    public class MvcFileUploadService : IFileUploadService
    {
        public const string basePath = "~/Content/CustomImages";

        public async Task<string> UploadFileAndGetUrl(Stream fileStream, string fileName)
        {
            var baseDir = HostingEnvironment.MapPath(basePath);
            var serverPath = Path.Combine(baseDir, fileName);
            
            using (var saveStream = new FileStream(serverPath, FileMode.Create))
            {
                await fileStream.CopyToAsync(saveStream);
            }

            var test = VirtualPathUtility.ToAbsolute(basePath);
            //todo : probably Path isn't good here
            return Path.Combine(test, fileName);
        }
    }
}