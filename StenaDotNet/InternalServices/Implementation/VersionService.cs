﻿using StenaDotNet.InternalServices.Interfaces;
using System.Reflection;

namespace StenaDotNet.InternalServices.Implementation
{
    public class VersionService : IVersionService
    {
        private readonly string version;

        public VersionService()
        {
            var currentAssembly = Assembly.GetExecutingAssembly();

            var assemblyName = currentAssembly.GetName();

            version = assemblyName.Version.ToString();
        }

        public string GetVersion()
        {
            return version;
        }
    }
}