﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StenaDotNet.InternalServices.Interfaces
{
    public interface IVersionService
    {
        string GetVersion();
    }
}
