﻿using AutoMapper;
using Services;
using Services.DTO.Walls;
using Services.Utility;
using Services.Walls;
using StenaDotNet.Areas.Admin.Models.WallContent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace StenaDotNet.Areas.Admin.Controllers
{
    [Authorize(Roles="admin")]
    public class WallContentController : Controller
    {
        private readonly WallService _wallService;
        private readonly WallCrudService _wallCrudService;
        private readonly IFileUploadService _fileUploadService;

        public WallContentController(WallService wallService, WallCrudService wallCrudService, 
            IFileUploadService fileUploadService)
        {
            _wallService = wallService;
            _wallCrudService = wallCrudService;
            _fileUploadService = fileUploadService;
        }

        // GET: Admin/WallContent
        public ActionResult Index()
        {
            var model = new WallListModel();

            model.ViewList = _wallCrudService.GetList();

            return View(model);
        }

        [HttpGet]
        public ActionResult AddEditWall(int? id)
        {
            if (id.HasValue)
            {
                var model = _wallCrudService.ViewWall(id.Value);
                if (model != null)
                {

                    var resultModel = Mapper.Map<WallModel>(model);

                    resultModel.Content = new List<WallContentModel>
                    {
                        new WallContentModel
                        {
                            ContentId = 1,
                            Name = "content1"
                        },

                        new WallContentModel
                        {
                            ContentId = 1,
                            Name = "content2"
                        }
                    };
                    return View("AddEditWall", resultModel);
                }
            }
            return View("AddEditWall", new WallModel());
        }
        
        [HttpPost]
        public JsonResult AddEditWall(WallModel wall)
        {
            if (ModelState.IsValid)
            {
                var editModel = Mapper.Map<EditWallDto>(wall);
                var result = _wallCrudService.CreateUpdateWall(editModel);
                //todo: better to change client location, not this
                var redirectUrl = wall.WallId == 0 ? Url.Action("AddEditWall", new { id = result.WallId}) : null;                
                return Json(new { success = true, redirect = redirectUrl });
            }

            return Json(new { success = false, reason = "Validation!"});
        }

        [HttpPost]
        public async Task<JsonResult> UploadImage(HttpPostedFileBase file)
        {
            var url = await _fileUploadService.UploadFileAndGetUrl(file.InputStream, file.FileName);
            return Json(new { success = true , url = url});
        }

    }
}