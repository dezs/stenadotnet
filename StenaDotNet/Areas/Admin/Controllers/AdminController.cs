﻿using AutoMapper;
using Services;
using StenaDotNet.Areas.Admin.Models.Admin;
using System.Web.Mvc;

namespace StenaDotNet.Areas.Admin.Controllers
{
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        private readonly WallService _wallService;


        public AdminController(WallService wallService)
        {
            _wallService = wallService;
        }

        // GET: Admin/Admin
        public ActionResult Index()
        {
            var sumDto = _wallService.GetSummary();

            var summary = Mapper.Map<SummaryModel>(sumDto);
            return View(summary);
        }
    }
}