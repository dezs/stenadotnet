﻿using Services.DTO.Walls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StenaDotNet.Areas.Admin.Models.WallContent
{
    public class WallListModel
    {
        public List<ViewWallDto> ViewList { get; set; }
    }
}