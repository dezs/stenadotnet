﻿using StenaDotNet.Utils.ValidationAttributes;
using StenaResources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace StenaDotNet.Areas.Admin.Models.WallContent
{
    public class WallModel
    {
        public int WallId { get; set; }

        public string Name { get; set; }
        
        [NgRequired]
        [Display(ResourceType = typeof(Walls), Name = "Latitude")]
        public double? Latitude { get; set; }

        [NgRequired]
        [Display(ResourceType = typeof(Walls), Name = "Longitude")]
        public double? Longitude { get; set; }

        public string FileUrl { get; set; }

        public List<WallContentModel> Content { get; set; }
    }

    public class WallContentModel
    {
        public int ContentId { get; set; }

        public string Name { get; set; }
    }
}