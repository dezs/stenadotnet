﻿namespace StenaDotNet.Areas.Admin.Models.Admin
{
    public class SummaryModel
    {
        public int WallCount { get; set; }

        public int WallContentCount { get; set; }
    }
}