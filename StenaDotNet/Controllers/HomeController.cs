﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StenaDotNet.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Partners()
        {
            return View();
        }

        public ActionResult HowItWorks()
        {
            return View();
        }
    }
}