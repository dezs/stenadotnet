﻿using StenaDotNet.Models.WallDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace StenaDotNet.Controllers
{
    public class WallDetailsController : Controller
    {
        // GET: WallDetails
        public ActionResult Index(string seoId)
        {
            var model = new WallDetailModel
            {
                PageTitle = seoId,
                PageContent = "Some content retrieved from database"
            };

            return View(model);
        }
    }
}