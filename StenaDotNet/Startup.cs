﻿using Microsoft.Owin;
using Owin;
using StenaDotNet.App_Start;
using System.Web.Mvc;

[assembly: OwinStartupAttribute(typeof(StenaDotNet.Startup))]
namespace StenaDotNet
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            InjectionConfig.RegisterOwin(app);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            ConfigureAuth(app);
        }
    }
}
