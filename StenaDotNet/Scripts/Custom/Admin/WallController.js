﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('WallController', WallController);

    WallController.$inject = ['$location', 'WallService', '$scope', 'fileUpload'];

    function WallController($location, wallService, $scope, fileUpload) {
        /* jshint validthis:true */
        var vm = this;

        vm.isLoading = false;

        vm.submit = function () {
            vm.isLoading = true;
            console.log("saving", vm.editModel);

            if (vm.editModel.PreviewFile != undefined) {
                fileUpload.upload(vm.editModel.PreviewFile)
                .then(function (responce) {
                    console.log('image url', responce.data.url);
                    vm.editModel.FileUrl = responce.data.url;
                    saveProceed();
                }, function (err) {
                    console.log('error when saving preview Url', err)
                });
            } else {

                saveProceed();
            }
            
            //todo: ladda when submt
        }

        vm.reset = function () {
            vm.editModel = angular.copy(vm.superModel);
        }

        activate();

        function activate() {
            vm.superModel = wallService.getModel();
            vm.reset();
        }

        function saveProceed() {
            wallService.saveModel(vm.editModel)
                .then(saveSuccess, saveError)
                .finally(function () {
                    console.log('always');
                    vm.isLoading = false;
                });
        }

        function saveSuccess(responce) {
            console.log("success", responce);
            var data  = responce.data;
            if (data.success === true) {
                if (data.redirect) {
                    location.href = data.redirect;
                }
                vm.superModel = angular.copy(vm.editModel);
            } else {
                console.log("validation on server occured!", data)
            }
        }

        function saveError(responce) {
            console.log("error when saving", responce);
        }
    }
})();
