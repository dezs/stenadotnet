﻿(function () {
    'use strict';

    angular
        .module('app')
        .provider('WallService', wallServiceProvider);
    
    function wallServiceProvider() {
        console.log('provider start')
        this.$get = WallService;

        var model = {};
        var postUrl;

        this.setupModel = function (modelJson) {
            console.log("model set", modelJson);
            model = modelJson;
        }

        this.setupSubmitUrl = function (url) {
            postUrl = url;
        }
        
        WallService.$inject = ['$http'];

        function WallService($http) {
            var serv = {};

            serv.getModel = function () {
                return model;
            }

            serv.saveModel = function (model) {
                console.log("saving model", model);
                return $http.post(postUrl, model);
            }


            return serv;
            
        }
    }
})();