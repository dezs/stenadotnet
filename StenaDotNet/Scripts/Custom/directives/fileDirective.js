﻿(function() {
    'use strict';

    angular
        .module('app')
        .directive('fileDirective', fileDirective);

    fileDirective.$inject = ['$window'];
    
    function fileDirective ($window) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
                fileDirective: '='
            }
        };
        return directive;

        function link(scope, element, attrs) {
            element.bind('change', function (event) {
                var file = event.target.files[0];
                scope.fileDirective = file ? file : undefined;
                scope.$apply();
            });
        }
    }

})();