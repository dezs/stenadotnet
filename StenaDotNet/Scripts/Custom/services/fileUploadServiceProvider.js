﻿(function () {
    'use strict';

    angular
        .module('app')
        .provider('fileUpload', fileUploadServiceProvider);
    
    function fileUploadServiceProvider() {
        console.log("file upload provider")
        this.$get = UploadService;

        var model = {};
        var postUrl;

        this.setupUploadUrl = function (url) {
            postUrl = url;
        }
        
        UploadService.$inject = ['$http'];

        function UploadService($http) {
            var serv = {};
            
            serv.upload = function (file) {
                console.log("saving file", file);
                var formData = new FormData();
                formData.append('file', file);
                return $http({
                    method: 'POST',
                    headers: {
                        'Content-Type': undefined
                    },
                    url: postUrl,
                    data: formData,
                    transformRequest: angular.identity
                });
            }

            return serv;
            
        }
    }
})();