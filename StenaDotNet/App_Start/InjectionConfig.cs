﻿using DataLayer;
using DataLayer.Models.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using NLog;
using Owin;
using Services;
using Services.Utility;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleInjector.Integration.Web.Mvc;
using StenaDotNet.InternalServices.Implementation;
using StenaDotNet.InternalServices.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web;
using System.Web.Mvc;

namespace StenaDotNet.App_Start
{
    public static class InjectionConfig
    {

        public static IDependencyResolver Register()
        {
            var container = new Container();
            ServiceInjectionConfig.Register(container);

            container.Register<ILogger>(() =>
            {
                var logger = LogManager.GetLogger("SingleLogger");

                return logger;
            }, Lifestyle.Singleton);
            //  container.Register<ILogger>(dependencyContext 
            //      => new LoggerAdapter(LogManager.GetLogger(dependencyContext.ImplementationType.FullName)));

            container.Register<IVersionService, VersionService>(Lifestyle.Singleton);
            container.Register<IFileUploadService, MvcFileUploadService>(Lifestyle.Singleton);

            container.RegisterPerWebRequest<ApplicationDbContext>();
            container.RegisterPerWebRequest<IUserStore<ApplicationUser>>(() =>
                    new UserStore<ApplicationUser>(container.GetInstance<ApplicationDbContext>()));
            container.RegisterPerWebRequest<IRoleStore<ApplicationRole, string>>(() =>
                    new RoleStore<ApplicationRole>(container.GetInstance<ApplicationDbContext>()));

            container.RegisterPerWebRequest<ApplicationUserManager>();

            container.RegisterPerWebRequest<ApplicationSignInManager>();

            container.RegisterPerWebRequest<IAuthenticationManager>(() => AdvancedExtensions.IsVerifying(container)
                ? new OwinContext(new Dictionary<string, object>()).Authentication
                : HttpContext.Current.GetOwinContext().Authentication);
            
            var resolver = new SimpleInjectorDependencyResolver(container);
            //DependencyResolver.SetResolver(resolver);

            return resolver;
        }

        public static void RegisterOwin(IAppBuilder app)
        {
            var container = ((SimpleInjectorDependencyResolver)DependencyResolver.Current).Container;

            container.RegisterInitializer<ApplicationUserManager>(a => a.Initializer(app));

            //init database lock container so we should do it after all registrations
            InitDatabase(container);

            container.Verify();
        }


        private static void InitDatabase(Container container)
        {
            //database init

            container.RegisterSingleton<ApplicationDbInitializer>();

            container.RegisterSingleton<Func<ApplicationUserManager>>(
                () => container.GetInstance<ApplicationUserManager>());
            container.RegisterSingleton<Func<ApplicationRoleManager>>(
                () => container.GetInstance<ApplicationRoleManager>());

            var dbInitializer = container.GetInstance<ApplicationDbInitializer>();
            Database.SetInitializer(dbInitializer);
        }
    }
}