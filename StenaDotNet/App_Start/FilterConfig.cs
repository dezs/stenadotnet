﻿using StenaDotNet.Utils.Filters;
using System.Web.Mvc;

namespace StenaDotNet
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());

            filters.Add(DependencyResolver.Current.GetService<VersionFilter>());
        }
    }
}
