﻿using AutoMapper;
using StenaDotNet.App_Start.MapperProfile;

namespace StenaDotNet.App_Start
{
    public static class MapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<WallMapProfile>();
            });
        }

    }
}