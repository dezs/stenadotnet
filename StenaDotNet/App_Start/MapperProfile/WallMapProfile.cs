﻿using AutoMapper;
using Services.DTO.Walls;
using StenaDotNet.Areas.Admin.Models.WallContent;
using Services.DTO;
using StenaDotNet.Areas.Admin.Models.Admin;

namespace StenaDotNet.App_Start.MapperProfile
{
    public class WallMapProfile : Profile
    {
        public WallMapProfile()
        {
            CreateMap<WallModel, EditWallDto>()
                .ForMember(x => x.PreviewUrl, o => o.MapFrom(x => x.FileUrl));

            CreateMap<EditWallDto, WallModel>()
                .ForMember(x => x.FileUrl, o => o.MapFrom(x => x.PreviewUrl));

            CreateMap<SummaryDto, SummaryModel>()
                .ForMember(x => x.WallCount, o => o.MapFrom(x => x.WallsCount))
                .ForMember(x => x.WallContentCount, o => o.MapFrom(x => x.ContentCount));

        }
    }
}