﻿using System.Web.Mvc;
using System.Web.Routing;

namespace StenaDotNet
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "WallDetails",
                url: "{controller}/{seoId}",
                defaults: new { action = "Index", seoId = UrlParameter.Optional},
                constraints: new { controller = "WallDetails" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
