﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StenaDotNet.Models.WallDetails
{
    public class WallDetailModel
    {
        public string PageTitle { get; set; }

        public string PageContent { get; set; }
    }
}