﻿namespace Utility
{
    public static class GeographyUtility
    {
        public static string GetPointText(double latitude, double longitude)
        {
            return string.Format("POINT({0} {1})", longitude, latitude).Replace(',','.');
        }
    }
}
