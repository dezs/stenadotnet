﻿namespace Utility
{
    public static class StringConstants
    {
        public const int MaxUrlLength = 2083;

        public const int CommonStringLength = 250;

        public const int CommonDescriptionLength = 2000;
    }
}
