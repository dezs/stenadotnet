﻿using DataLayer.Migrations;
using DataLayer.Models.Identity;
using Microsoft.AspNet.Identity;
using System;
using System.Data.Entity;

namespace DataLayer
{
    public class ApplicationDbInitializer : MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>
    {
        private readonly Func<ApplicationRoleManager> roleManagerFactory;
        private readonly Func<ApplicationUserManager> userManagerFactory;

        public ApplicationDbInitializer(Func<ApplicationUserManager> userManagerFactory,
            Func<ApplicationRoleManager> roleManagerFactory)
        {
            this.userManagerFactory = userManagerFactory;
            this.roleManagerFactory = roleManagerFactory;
        }

        public override void InitializeDatabase(ApplicationDbContext context)
        {
            InitializeIdentityForEF(context);
            base.InitializeDatabase(context);
        }

        public void InitializeIdentityForEF(ApplicationDbContext db)
        {
            const string adminName = "dezskk@gmail.com";
            const string password = "test322pass";
            const string roleName = "admin";

            // get a instance from the factory
            var userManager = this.userManagerFactory.Invoke();
            var roleManager = this.roleManagerFactory.Invoke();

            var adminRole = roleManager.FindByName(roleName);

            if (adminRole == null)
            {
                adminRole = new ApplicationRole() { Name = roleName };
                roleManager.Create(adminRole);
            }

            var adminUser = userManager.FindByEmail(adminName);

            if (adminUser == null)
            {
                adminUser = new ApplicationUser { Email = adminName, UserName = adminName };
                userManager.Create(adminUser, password);
            }

            var rolesForUser = userManager.GetRoles(adminUser.Id);

            if (!rolesForUser.Contains(adminRole.Name))
            {
                var result = userManager.AddToRole(adminUser.Id, adminRole.Name);
            }

            db.SaveChanges();
        }

    }
}
