﻿using DataLayer.Models.CommonContent;
using System.Data.Entity.ModelConfiguration;
using Utility;

namespace DataLayer.Mapping.CommonContent
{
    class ImageDataMap : EntityTypeConfiguration<ImageData>
    {
        public ImageDataMap()
        {
            Property(x => x.ImageUrl)
                .IsRequired()
                .HasMaxLength(StringConstants.MaxUrlLength);

            Property(x => x.ImageAlt)
                .HasMaxLength(StringConstants.CommonStringLength);
        }
    }
}
