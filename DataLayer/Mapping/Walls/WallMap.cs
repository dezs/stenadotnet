﻿using DataLayer.Models.Walls;
using System.Data.Entity.ModelConfiguration;
using Utility;

namespace DataLayer.Mapping.Walls
{
    class WallMap : EntityTypeConfiguration<Wall>
    {
        public WallMap()
        {
            Property(x => x.Name)
                .HasMaxLength(StringConstants.CommonStringLength);

            Property(x => x.Location)
                .IsRequired();

            HasRequired(x => x.Preview)
                .WithMany()
                .HasForeignKey(x => x.PreviewId)
                .WillCascadeOnDelete(false);

            HasMany(x => x.WallContents)
                .WithRequired(x => x.Wall);
        }
    }
}
