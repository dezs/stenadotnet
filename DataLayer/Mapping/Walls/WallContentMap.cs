﻿using DataLayer.Models.Walls;
using System.Data.Entity.ModelConfiguration;
using Utility;

namespace DataLayer.Mapping.Walls
{
    class WallContentMap : EntityTypeConfiguration<WallContent>
    {
        public WallContentMap()
        {
            Property(x => x.Name)
                .IsRequired()
                .HasMaxLength(StringConstants.CommonStringLength);

            Property(x => x.Details)
                .IsRequired()
                .HasMaxLength(StringConstants.CommonDescriptionLength);

            HasRequired(x => x.Preview)
                .WithMany()
                .HasForeignKey(x => x.PreviewId)
                .WillCascadeOnDelete(false);
        }
    }
}
