﻿using DataLayer.Models.CommonContent;
using System.Collections.ObjectModel;
using System.Data.Entity.Spatial;

namespace DataLayer.Models.Walls
{
    public class Wall
    {
        public int WallId { get; set; }

        public string Name { get; set; }

        public DbGeography Location { get; set; }

        public int PreviewId { get; set; }

        public virtual ImageData Preview { get; set; }

        public virtual Collection<WallContent> WallContents { get; set; }
    }
}
