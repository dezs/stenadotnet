﻿using DataLayer.Models.CommonContent;

namespace DataLayer.Models.Walls
{
    public class WallContent
    {
        public int WallContentId { get; set; }

        public string Name { get; set; }
        //todo
        public string Details { get; set; }

        public int WallId { get; set; }

        public virtual Wall Wall { get; set; }

        public int PreviewId { get; set; }

        public virtual ImageData Preview { get; set; }
    }
}
