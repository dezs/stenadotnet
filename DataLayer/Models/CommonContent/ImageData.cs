﻿namespace DataLayer.Models.CommonContent
{
    public class ImageData
    {
        public int ImageDataId { get; set; }

        public string ImageUrl { get; set; }

        public string ImageAlt { get; set; }
    }
}
