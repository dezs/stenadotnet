﻿using Services.Utility;
using SimpleInjector;

namespace Services
{
    public static class ServiceInjectionConfig
    {
        public static void Register(Container container)
        {
            container.Register<WallService>();
        }

    }
}
