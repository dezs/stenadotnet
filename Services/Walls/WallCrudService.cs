﻿using DataLayer;
using DataLayer.Models.CommonContent;
using DataLayer.Models.Walls;
using Services.DTO.Walls;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.Linq;
using Utility;

namespace Services.Walls
{
    public class WallCrudService
    {
        private readonly ApplicationDbContext _context;

        public WallCrudService(ApplicationDbContext context)
        {
            _context = context;
        }

        public EditWallDto ViewWall(int id)
        {
            //todo: need separate dto to view
            return _context.Walls.Select(x => new EditWallDto
            {
                Name = x.Name,
                WallId = x.WallId,
                Latitude = x.Location.Latitude ?? 0,
                Longitude = x.Location.Longitude ?? 0,
                PreviewUrl = x.Preview.ImageUrl
            }).FirstOrDefault(x => x.WallId == id);
        }

        public List<ViewWallDto> GetList(int take = 20, int skip = 0)
        {
            var query = _context.Walls.AsQueryable();

            if (skip > 0)
            {
                query = query.Skip(skip);
            }

            if(take > 0)
            {
                query = query.Take(take);
            }

            var finalQuery = query.Select(x => new ViewWallDto
            {
                Latitude = x.Location.Latitude,
                Longitude = x.Location.Longitude,
                Name = x.Name,
                PreviewUrl = x.Preview.ImageUrl,
                PreviewAlt = x.Preview.ImageAlt,
                WallId = x.WallId,
                ContentCount = x.WallContents.Count()
            });

            return finalQuery.ToList();
        }

        public EditWallDto CreateUpdateWall(EditWallDto wall)
        {
            var locationText = GeographyUtility.GetPointText(wall.Latitude, wall.Longitude);

            var location = DbGeography.FromText(locationText);

            var wallName = wall.Name ?? "Untitled " + locationText;

            var model = wall.WallId != 0 ? _context.Walls.FirstOrDefault(x => x.WallId == wall.WallId) : null;

            model = model ?? new Wall();

            model.Location = location;

            model.Name = wallName;

            model.Preview = model.Preview ?? new ImageData();

            model.Preview.ImageAlt = wallName;
            model.Preview.ImageUrl = wall.PreviewUrl;

            if (model.WallId == 0)
            {
                _context.Walls.Add(model);
            }

            _context.SaveChanges();

            wall.WallId = model.WallId;
            wall.Name = wallName;

            return wall;
        }

    }
}
