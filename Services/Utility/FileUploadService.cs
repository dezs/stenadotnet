﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Utility
{
    public class FileUploadService : IFileUploadService
    {
        public FileUploadService()
        {
        }

#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        public async Task<string> UploadFileAndGetUrl(Stream fileStream, string fileName)
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            throw new NotImplementedException();
        }
    }

    public interface IFileUploadService
    {
        Task<string> UploadFileAndGetUrl(Stream fileStream, string fileName);
    }
}
