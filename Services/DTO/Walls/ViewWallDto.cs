﻿namespace Services.DTO.Walls
{
    public class ViewWallDto
    {
        public int WallId { get; set; }

        public string Name { get; set; }

        public double? Latitude { get; set; }

        public double? Longitude { get; set; }

        public string PreviewUrl { get; set; }

        public string PreviewAlt { get; set; }

        public int ContentCount { get; set; }
    }
}
