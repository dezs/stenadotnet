﻿namespace Services.DTO.Walls
{
    public class EditWallDto
    {
        public int WallId { get; set; }

        public string Name { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string PreviewUrl { get; set; }
    }
}
