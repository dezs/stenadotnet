﻿namespace Services.DTO
{
    public class SummaryDto
    {
        public int WallsCount { get; set; }

        public int ContentCount { get; set; }
    }
}
