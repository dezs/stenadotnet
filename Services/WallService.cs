﻿using DataLayer;
using Services.DTO;
using System.Linq;

namespace Services
{
    public class WallService
    {
        private readonly ApplicationDbContext _context;

        public WallService(ApplicationDbContext context)
        {
            _context = context;
        }

        public SummaryDto GetSummary()
        {
            return new SummaryDto
            {
                WallsCount = _context.Walls.Count(),
                ContentCount = _context.WallContents.Count()
            };
        }

    }
}
